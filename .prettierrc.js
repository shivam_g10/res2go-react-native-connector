module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: false,
  arrowParens: 'avoid',
  useTabs: false,
  tabWidth: 4
};
