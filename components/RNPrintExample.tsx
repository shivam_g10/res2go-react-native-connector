/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component, useEffect, useState} from 'react';
import {
    AppRegistry,
    Button,
    StyleSheet,
    NativeModules,
    Platform,
    Text,
    View,
    Alert,
    TouchableOpacity,
} from 'react-native';

import { BLEPrinter } from 'react-native-thermal-receipt-printer';
interface IBLEPrinter {
    device_name: string;
    inner_mac_address: string;
  }

export default function RNPrintExample(props: any) : any {
const [printers, setPrinters] = useState([]);
  const [currentPrinter, setCurrentPrinter] = useState();
  const initialize = () =>{
    console.log('Called initialize');
    BLEPrinter.init().then(()=> {
      BLEPrinter.getDeviceList().then((data: any) => {
        console.log('DEVICE LIST code run');
        console.log(data);
        Alert.alert(JSON.stringify(data));
        setPrinters(data);
      }).catch((e) => {
        Alert.alert(e.message);
      });
    });
  };

  const _connectPrinter = (printer: any) => {
    //connect printer
    BLEPrinter.connectPrinter(printer.inner_mac_address).then(
      (data: any) => {
        setCurrentPrinter(data);
      },
      error => console.warn(error))
  }

  const printTextTest = () => {
    currentPrinter && BLEPrinter.printText("<C>sample text</C>\n");
  }

  const printBillTest = () => {
    currentPrinter && BLEPrinter.printBill("<C>sample bill</C>");
  }

  return (
    <View style={styles.container}>
      {
        printers.map((printer: any) => (
          <TouchableOpacity key={printer.inner_mac_address} onPress={() => _connectPrinter(printer)}>
            {`device_name: ${printer.device_name}, inner_mac_address: ${printer.inner_mac_address}`}
          </TouchableOpacity>
          ))
      }
      <TouchableOpacity onPress={initialize}>
        <Text>Initialize printers</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={printTextTest}>
        <Text>Print Text</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={printBillTest}>
        <Text>Print Bill Text</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});
