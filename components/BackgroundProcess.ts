import BackgroundService from 'react-native-background-actions';
const LINK = 'wss://kalendars.io/socket/';
export default async function BackgroundTask()
{
    console.log('BackgroundTask setup called');
    const sleep = (time: number | undefined) => new Promise<void>((resolve) => setTimeout(() => resolve(), time));

    const connectSocket = () =>
    {
        try
        {
            console.log('Connecting to socket');
            const socket = new WebSocket(LINK, null, {
                headers: {
                  "User-Agent": "react-native",
                },
              });
            socket.onopen = () =>
            {
                console.log('Socket is open');
            };

            socket.onmessage = (e) =>
            {
                try
                {
                    const data = JSON.parse(e.data);
                    console.log(data);
                }
                catch (e)
                {
                    console.log(e);
                }
            };

            socket.onerror = (e) =>
            {
                console.log('ERROR IN SOCKET');
                console.log(e);
                console.log('Reconnecting');
                connectSocket();
            };

            socket.onclose = () =>
            {
                console.log('SOCKET CLOSED');
                console.log('Reconnecting');
                connectSocket();
            }
        }
        catch(e)
        {
            console.log('Error setting up websocket');
            console.log('Retrying');
            connectSocket();
        }
    };
    // You can do anything in your task such as network requests, timers and so on,
    // as long as it doesn't touch UI. Once your task completes (i.e. the promise is resolved),
    // React Native will go into "paused" mode (unless there are other tasks running,
    // or there is a foreground app).
    const task = async (taskDataArguments: any) =>
    {
        try
        {
            connectSocket();
        }
        catch (e) 
        {
            console.log('Error setting up websocket main');
        }

    };

    const options = {
        taskName: 'Websocket',
        taskTitle: 'Talk to websocket',
        taskDesc: 'ExampleTask description',
        taskIcon: {
            name: 'ic_launcher',
            type: 'mipmap',
        },
        color: '#ff00ff',
    };

    await BackgroundService.start(task, options);
    await BackgroundService.stop();
}